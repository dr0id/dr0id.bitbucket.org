examples Package
================

:mod:`examples` Package
-----------------------

.. automodule:: examples
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    examples.TestHFSM
    examples.mousefollower
    examples.testablestatemachines

