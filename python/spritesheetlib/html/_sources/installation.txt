============
Installation
============

At the command line::

    $ easy_install spritesheetlib

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv spritesheetlib
    $ pip install spritesheetlib